sbuild
=======
Test run a SlackBuild in an isolated chroot/overlay: see what would have changed on the real system but also get an installable package if the script is successful. `sbuild` will also try to give some useful information in the exit code.

Requirements
------------
- A separate partition to mount the overlay-fs on (e.g., `/tmp` or `/var/run` on `tmpfs`, or user-specified).
- The script must be run as `root`.

Installation
------------
Put `sbuild` in your `PATH` somewhere (like `/usr/local/sbin`).

Usage
-----
Run `sbuild` in a directory with the SlackBuild (and info file) that you want to build. If the source files are not in the working directory, `sbuild` will try to download them first and compare against the listed checksums. Additional options:

- `-i` for an interactive shell to try things out (any changes will only persist in the overlay).
- `-c` to clean up the work directory for the current SlackBuild. This means delete the temporary overlay-fs, as well as all downloads in the current working directory (i.e., those listed in the SlackBuild's info file).
- `-n` to skip the info file check.
- `-m MOUNTPOINT` to select your own mountpoint for the overlay-fs (`/tmp/` or `/var/run` are tried otherwise).

Exit codes
----------
1. General errors.
2. The build succeeds but writes files outside of `/tmp`.
3. The build fails.
4. The build fails and writes files outside of `/tmp`.
